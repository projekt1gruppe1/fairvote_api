# fairVote API by gruppe1
Contributors:
- Rolf Isler
- Belana Kammermann
- Bardh Uruci
- Simon Zeltner
- Dominik Vollmer

To start up the application, import the dependencies and run "clean spring-boot:run"

This is a RESTful API which is consumed by the fairVote Application (https://fairvote.ch). It provides the application with data such as current and past proposals (nationale Abstimmungen), statistical data like the result and demographic data and the functionality to cast a vote. Due to the scope of the project, there are no security features implemented, such as authentication of users and verification of votes.The admin-functions are not secured, as well. For more details about the project and its scope, check out the documentation delivered with the first assignment in the module "Information Management".