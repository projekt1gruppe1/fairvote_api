package gruppe1.fairvote.controller;

import gruppe1.fairvote.models.Permission;
import gruppe1.fairvote.repositories.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(path = "/permissions")
public class PermissionController {
    @Autowired
    private PermissionRepository repository;

    /**
     * get all permissions
     * @return list of permissions
     */
    @GetMapping ("")
    List<Permission> getAllPermissions() {return repository.findAll();}

    /**
     * get permission by id
     * @param id unique identifier
     * @return the permission
     */
    @GetMapping ("/{id}")
    Optional<Permission> getPermission(@PathVariable Long id) {
    return repository.findById(id);
    }
}