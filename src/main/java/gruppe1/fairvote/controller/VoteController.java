package gruppe1.fairvote.controller;

import gruppe1.fairvote.models.ProposalResult;
import gruppe1.fairvote.models.Vote;
import gruppe1.fairvote.repositories.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@CrossOrigin
@RequestMapping (path = "/votes")
public class VoteController {
    @Autowired
    private VoteRepository repository;

    /**
     * gather information about a proposal (Abstimmung) for statistical purposes
     * @param proposalId identifier of a proposal
     * @return a json with the proposal results
     */
    @GetMapping("/results/{proposalId}")
    ProposalResult getProposalResults(@PathVariable Long proposalId) {
        List<Vote> votes = repository.findProposalBySystemName(proposalId);

        ProposalResult result = new ProposalResult();

        // set total count of voters
        result.setTotal(votes.size());

        // set total count of yes-votes
        result.setYes(votes.stream().filter((vote) -> vote.getDecision() == 1).count());

        // set total count of male voters (female voters are implied from this result)
        result.setMale(votes.stream().filter((vote) -> vote.getGender().equals("m")).count());

        // set count of voters per age group
        List<Long> agelist = new ArrayList<>();
        agelist.add(votes.stream().filter((vote) -> vote.getAgeGroup().equals("18-29")).count());
        agelist.add(votes.stream().filter((vote) -> vote.getAgeGroup().equals("30-39")).count());
        agelist.add(votes.stream().filter((vote) -> vote.getAgeGroup().equals("40-64")).count());
        agelist.add(votes.stream().filter((vote) -> vote.getAgeGroup().equals("65+")).count());
        result.setAge(agelist);

        return result;
    }

    /**
     * count yes-votes per canton (for statistics)
     * @param proposalId identifier of a proposal
     * @param cantonId two letter code of canton (e.g. ZH)
     * @return the total count of yes votes per canton
     */
    @GetMapping("/results/{proposalId}/canton/{cantonId}")
    Long getVotesByCanton(@PathVariable Long proposalId,@PathVariable String cantonId) {
        List<Vote> votes = repository.findVoteByProposalandCanton(proposalId,cantonId);
        return votes.stream().filter((vote) -> vote.getDecision() == 1).count();
    }

    /**
     * cast a vote
     * @param vote vote object with decision and statistical data
     * @return HTTP OK
     */
    @PostMapping("")
    public ResponseEntity castVote(@RequestBody Vote vote) {
        repository.save(vote);
        return ok().build();
    }
}
