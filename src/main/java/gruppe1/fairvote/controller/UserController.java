package gruppe1.fairvote.controller;

import gruppe1.fairvote.controller.exceptions.LoginFailedException;
import gruppe1.fairvote.controller.exceptions.UserCreationException;
import gruppe1.fairvote.controller.exceptions.UserNotFoundException;
import gruppe1.fairvote.models.LoginRequest;
import gruppe1.fairvote.models.User;
import gruppe1.fairvote.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@CrossOrigin
@RequestMapping(path="/users")
public class UserController {
    @Autowired
    private UserRepository repository;

    /**
     * logs in a user
     * @param loginRequest request-object containing username (email) & password
     * @return the user
     */
    @PostMapping("/login")
    User login(@RequestBody LoginRequest loginRequest) {
        User user = repository.findByEmail(loginRequest.getEmail()).orElseThrow(() -> new UserNotFoundException(loginRequest.getEmail()));

        if(user.getPassword().equals(loginRequest.getPassword())){
            user.setPassword(""); // reset password from response for security reasons
            return user;
        }
        throw new LoginFailedException(loginRequest.getEmail());
    }

    //admin functions
    /**
     * list all users (admin permissions needed)
     * @return list of all users
     */
    @GetMapping("/")
    List<User> getAllUsers() {
        return repository.findAll();
    }

    /**
     * get a user by id (admin permissions needed)
     * @param userId unique user identifier
     * @return user
     */
    @GetMapping("/{id}")
    User getUserById(@PathVariable("id") Long userId) {
        return repository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
    }

    /**
     * creates a new user
     * @param user user
     * @param request current request
     * @return HTTP CREATED
     */
    @PostMapping("")
    public ResponseEntity save(@RequestBody User user, HttpServletRequest request) {
        if(repository.findByEmail(user.getEmail()).isPresent())
            throw new UserCreationException(user.getEmail());

        User userToCreate = repository.save(user);
        return created(
                ServletUriComponentsBuilder
                        .fromContextPath(request)
                        .path("/proposals/{id}")
                        .buildAndExpand(userToCreate.getId())
                        .toUri())
                .build();
    }

    /**
     * update a user (admin permissions needed)
     * @param userId unique user identifier
     * @param user user
     * @return HTTP OK
     */
    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable("id") Long userId, @RequestBody User user) {
        repository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        repository.save(user);
        return ok().build();
    }

    /**
     * deletes a user (admin permissions needed)
     * @param userId unique user identifier
     * @return HTTP OK
     */
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long userId) {
        User userToDelete = repository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        repository.delete(userToDelete);
        return ok().build();
    }
}

