package gruppe1.fairvote.controller.exceptions;

public class LoginFailedException extends RuntimeException {
    public LoginFailedException(String email) {
        super("Login failed for user " + email);
    }
}
