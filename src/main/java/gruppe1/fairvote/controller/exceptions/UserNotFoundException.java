package gruppe1.fairvote.controller.exceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(Long id) {
        super("Could not find user " + id);
    }
    public UserNotFoundException(String email) {
        super("Could not find user " + email);
    }
}
