package gruppe1.fairvote.controller.exceptions;

public class ProposalNotFoundException extends RuntimeException {
    public ProposalNotFoundException(String systemName){
        super("Could not find proposal " + systemName);
    }
}