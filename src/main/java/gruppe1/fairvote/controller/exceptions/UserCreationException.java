package gruppe1.fairvote.controller.exceptions;

public class UserCreationException extends RuntimeException {
    public UserCreationException(Object email) {
        super("User already exists " + email);
    }
}
