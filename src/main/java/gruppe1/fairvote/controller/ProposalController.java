package gruppe1.fairvote.controller;

import gruppe1.fairvote.controller.exceptions.ProposalNotFoundException;
import gruppe1.fairvote.models.Proposal;
import gruppe1.fairvote.repositories.ProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

import static org.springframework.http.ResponseEntity.*;


@RestController
@CrossOrigin
@RequestMapping(path="/proposals")
public class ProposalController {
    @Autowired
    private ProposalRepository repository;

    /**
     * list all recent proposals (status 20)
     * @return list of recent proposals
     */
    @GetMapping("/recent")
    Collection<Proposal> getRecentProposals() {
        return repository.findProposalsByStatus(20);
    }

    /**
     * list all archived proposals (status 30)
     * @return list of archived proposals
     */
    @GetMapping("/archived")
    Collection<Proposal> getArchivedroposals() {
        return repository.findProposalsByStatus(30);
    }

    /**
     * get a proposal by its system name
     * @param systemName unique system name (used for routing in the frontend)
     * @return the proposal
     */
    @GetMapping("/{systemName}")
    Proposal getProposalBySystemName(@PathVariable("systemName") String systemName) {
        return repository.findProposalBySystemName(systemName).orElseThrow(() -> new ProposalNotFoundException(systemName));
    }

    //admin functions
    /**
     * create a new proposal
     * @param proposal proposal object
     * @param request current request
     * @return HTTP CREATED
     */
    @PostMapping("")
    public ResponseEntity save(@RequestBody Proposal proposal, HttpServletRequest request) {
        Proposal proposalToCreate = repository.save(proposal);
        return created(
                ServletUriComponentsBuilder
                        .fromContextPath(request)
                        .path("/proposals/{id}")
                        .buildAndExpand(proposalToCreate.getSystemName())
                        .toUri())
                .build();
    }

    /**
     * update an existing proposal
     * @param systemName unique system name
     * @param proposal proposal object
     * @return HTTP OK
     */
    @PutMapping("/{systemName}")
    public ResponseEntity update(@PathVariable("systemName") String systemName, @RequestBody Proposal proposal) {
        repository.findProposalBySystemName(systemName).orElseThrow(() -> new ProposalNotFoundException(systemName));
        repository.save(proposal);
        return ok().build();
    }

    /**
     * delete a proposal
     * @param systemName unique system name
     * @return HTTP OK
     */
    @DeleteMapping("/{systemName}")
    public ResponseEntity delete(@PathVariable("systemName") String systemName) {
        Proposal proposalToDelete = repository.findProposalBySystemName(systemName).orElseThrow(() -> new ProposalNotFoundException(systemName));
        repository.delete(proposalToDelete);
        return ok().build();
    }
}
