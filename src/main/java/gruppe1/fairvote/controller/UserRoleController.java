package gruppe1.fairvote.controller;

import gruppe1.fairvote.models.UserRole;
import gruppe1.fairvote.repositories.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(path = "/userRoles")
public class UserRoleController {
    @Autowired
    private UserRoleRepository repository;

    /**
     * list of all user roles
     * @return list of user roles
     */
    @GetMapping("")
    List<UserRole> getAllUserRoles() {
        return repository.findAll();
    }

    /**
     * get user role by id
     * @param id unique identifier
     * @return the user role
     */
    @GetMapping("/{id}")
    Optional<UserRole> getUserRole(@PathVariable Long id){
        return repository.findById(id);
    }
}
