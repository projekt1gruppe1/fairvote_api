package gruppe1.fairvote.repositories;

import gruppe1.fairvote.models.Proposal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;

public interface ProposalRepository extends JpaRepository<Proposal, Long> {
    @Query("SELECT p FROM Proposal p WHERE p.Status = :status")
    Collection<Proposal> findProposalsByStatus(@Param("status") int status);

    @Query("SELECT p FROM Proposal p WHERE p.SystemName = :systemName")
    Optional<Proposal> findProposalBySystemName(@Param("systemName") String systemName);
}
