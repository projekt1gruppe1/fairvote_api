package gruppe1.fairvote.repositories;

import gruppe1.fairvote.models.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionRepository extends JpaRepository<Permission, Long> {
}
