package gruppe1.fairvote.repositories;

import gruppe1.fairvote.models.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface VoteRepository extends JpaRepository<Vote, Long> {
    @Query("SELECT v FROM Vote v WHERE v.ProposalId = :ProposalId")
    List<Vote> findProposalBySystemName(@Param("ProposalId") Long ProposalId);

    @Query("SELECT v FROM Vote v WHERE v.ProposalId = :ProposalId and v.CantonOfResidence = :CantonOfResidence")
    List<Vote> findVoteByProposalandCanton(@Param("ProposalId") Long ProposalId,@Param("CantonOfResidence") String CantonOfResidence);
}
