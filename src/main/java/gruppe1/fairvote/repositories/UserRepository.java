package gruppe1.fairvote.repositories;

import gruppe1.fairvote.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT u FROM User u WHERE u.Email = :email")
    Optional<User> findByEmail(@Param("email") String email);
}
