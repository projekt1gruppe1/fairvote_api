package gruppe1.fairvote.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String Name;

    @ManyToMany
    @JoinTable(
            name = "permission_role_mapping",
            joinColumns = @JoinColumn(name = "UserRole_Id"),
            inverseJoinColumns = @JoinColumn(name = "Permission_Id"))
    Set<Permission> permissions;
}
