package gruppe1.fairvote.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
public class Proposal {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long Id;

    @NotNull
    private String SystemName;

    @NotNull
    private String Name;

    @NotNull
    private String ShortDescription;

    @NotNull
    @Column(columnDefinition="TEXT")
    private String Content;

    @NotNull
    private String Question;

    @NotNull
    private Date EffectiveDate;

    private String Link;

    @NotNull
    private Integer Status;

    @NotNull
    private Timestamp CreatedOnUtc;
}
