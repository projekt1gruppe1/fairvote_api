package gruppe1.fairvote.models;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Vote {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long Id;

    private long ProposalId;

    private String Code;

    private long Decision;

    private String AgeGroup;

    private String Gender;

    private String CantonOfResidence;

}
