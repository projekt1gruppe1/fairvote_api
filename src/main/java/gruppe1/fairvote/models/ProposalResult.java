package gruppe1.fairvote.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
public class ProposalResult {
    private Integer total;
    private Long yes;
    private Long male;
    private List<Long> age;
}



