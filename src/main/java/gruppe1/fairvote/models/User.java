package gruppe1.fairvote.models;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;


@Data
@Entity
public class User {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private long Id;

    private String Email;

    private String Password;

    private String Name;

    private String Gender;

    private String CantonOfResidence;

    private Date DateOfBirth;

    @ManyToMany
    @JoinTable(
            name = "user_userrole_mapping",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "userrole_id"))
    Set<UserRole> roles;
}
