package gruppe1.fairvote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FairvoteApplication {
	public static void main(String[] args) {
		SpringApplication.run(FairvoteApplication.class, args);
	}
}
